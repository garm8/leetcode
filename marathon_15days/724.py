# 724. Find Pivot Index
#
# Given an array of integers nums, calculate the pivot index of this array.
#
# The pivot index is the index where the sum of all the numbers strictly to the left of the index is equal to the sum of
# all the numbers strictly to the index's right.
#
# If the index is on the left edge of the array, then the left sum is 0 because there are no elements to the left. This
# also applies to the right edge of the array.
#
# Return the leftmost pivot index. If no such index exists, return -1.


import typing as t
import pytest


class Solution:
    def pivotIndex_head_on(self, nums: t.List[int]) -> int:
        for count, num in enumerate(nums, start=0):
            if sum(nums[:count]) == sum(nums[count + 1:]):
                return count

        return -1

    def pivotIndex(self, nums: t.List[int]) -> int:
        right_sum = sum(nums[1:])
        left_sum = 0

        for count, num in enumerate(nums, start=0):
            if left_sum == right_sum:
                return count

            if count != len(nums) - 1:
                left_sum += num
                right_sum -= nums[count + 1]

        return -1


@pytest.mark.parametrize(
    'input_data, result',
    (
            ([1, 7, 3, 6, 5, 6], 3),
            ([1, 2, 3], -1),
            ([2, 1, -1], 0),
    )
)
def test_solution(input_data, result):
    solution = Solution()

    assert solution.pivotIndex(input_data) == result
