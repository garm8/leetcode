# 1480. Running Sum of 1d Array
#
# Given an array nums. We define a running sum of an array as runningSum[i] = sum(nums[0]…nums[i]).
# Return the running sum of nums.


import typing as t
import pytest


class Solution:
    def runningSum(self, nums: t.List[int]) -> t.List[int]:
        result = []
        sum_ = 0
        for i in nums:
            sum_ += i
            result.append(sum_)
        return result


@pytest.mark.parametrize(
    'input_data, result',
    (
            ([1, 2, 3, 4], [1, 3, 6, 10]),
            ([1, 1, 1, 1, 1], [1, 2, 3, 4, 5]),
            ([3, 1, 2, 10, 1], [3, 4, 6, 16, 17]),
    )
)
def test_solution(input_data, result):
    solution = Solution()

    assert solution.runningSum(input_data) == result
