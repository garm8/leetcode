# 392. Is Subsequence
#
# Given two strings s and t, return true if s is a subsequence of t, or false otherwise.
#
# A subsequence of a string is a new string that is formed from the original string by deleting some (can be none) of
# the characters without disturbing the relative positions of the remaining characters. (i.e., "ace" is a subsequence of
# "abcde" while "aec" is not).


import pytest


class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        index = 0
        result = [False for _ in range(len(s))]
        for count, s_symb in enumerate(s, start=0):
            while index != len(t):
                if s_symb == t[index]:
                    index += 1
                    result[count] = True
                    break
                else:
                    index += 1
        return all(result)


@pytest.mark.parametrize(
    's, t, result',
    (
            ('abc', 'ahbgdc', True),
            ('axc', 'ahbgdc', False),
    )
)
def test_solution(s, t, result):
    solution = Solution()

    assert solution.isSubsequence(s, t) == result
