# 205. Isomorphic Strings
#
# Given two strings s and t, determine if they are isomorphic.
#
# Two strings s and t are isomorphic if the characters in s can be replaced to get t.
#
# All occurrences of a character must be replaced with another character while preserving the order of characters. No
# two characters may map to the same character, but a character may map to itself.


import pytest


class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        s_dict = {}
        t_dict = {}

        for s_symb, t_symb in zip(s, t):
            if s_symb in s_dict and s_dict[s_symb] != t_symb or t_symb in t_dict and t_dict[t_symb] != s_symb:
                return False

            s_dict[s_symb] = t_symb
            t_dict[t_symb] = s_symb

        return True


@pytest.mark.parametrize(
    's, t, result',
    (
            ('egg', 'add', True),
            ('foo', 'bar', False),
            ('paper', 'title', True),
            ('badc', 'baba', False),
    )
)
def test_solution(s, t, result):
    solution = Solution()

    assert solution.isIsomorphic(s, t) == result
