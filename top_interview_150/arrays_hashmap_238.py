"""
Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of
nums except nums[i].

The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

You must write an algorithm that runs in O(n) time and without using the division operation.
"""

import typing as t
import pytest


class Solution:
    def productExceptSelf(self, nums: t.List[int]) -> t.List[int]:
        length = len(nums)
        result = [1] * length
        prefix = 1
        postfix = 1

        for i in range(length):
            result[i] *= prefix
            prefix *= nums[i]
        for i in range(length - 1, -1, -1):
            result[i] *= postfix
            postfix *= nums[i]

        return result


@pytest.mark.parametrize(
    'nums, result',
    (
            ([1,2,3,4], [24,12,8,6]),
            ([-1,1,0,-3,3], [0,0,9,0,0]),

    )
)
def test_solution(nums, result):
    solution = Solution()

    assert solution.productExceptSelf(nums) == result
