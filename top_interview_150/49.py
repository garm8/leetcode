"""
Given an array of strings strs, group the anagrams together. You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the
original letters exactly once.
"""

import typing as t
from collections import defaultdict
import pytest


class Solution:
    # def groupAnagrams(self, strs: t.List[str]) -> t.List[t.List[str]]:
    #     hash_map = defaultdict(list)
    #
    #     for word in strs:
    #         key = 26 * [0]  # a - z
    #
    #         for letter in word:
    #             key[ord(letter) - ord('a')] += 1
    #
    #         hash_map[tuple(key)].append(word)
    #
    #     return list(hash_map.values())

    def groupAnagrams(self, strs: t.List[str]) -> t.List[t.List[str]]:
        hash_map = dict()

        for word in strs:
            key = ''.join(sorted(word))
            if key in hash_map:
                hash_map[key].append(word)
            else:
                hash_map[key] = [word]

        return list(hash_map.values())


@pytest.mark.parametrize(
    'strs, result',
    (
            (["eat","tea","tan","ate","nat","bat"], [["bat"],["nat","tan"],["ate","eat","tea"]]),
            ([""], [[""]]),
            (["a"], [["a"]]),
    )
)
def test_solution(strs, result):
    solution = Solution()

    assert sorted(solution.groupAnagrams(strs)) == sorted(result)
