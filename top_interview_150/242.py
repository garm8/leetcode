"""
Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the
original letters exactly once.
"""

import pytest


class Solution:
    # def isAnagram(self, s: str, t: str) -> bool:
    #     if len(s) != len(t):
    #         return False
    #
    #     hash_map_s = dict()
    #     hash_map_t = dict()
    #
    #     for i, j in zip(s, t):
    #         if i == j:
    #             continue
    #         else:
    #             if i not in hash_map_s:
    #                 hash_map_s[i] = 1
    #             else:
    #                 hash_map_s[i] += 1
    #
    #             if j not in hash_map_t:
    #                 hash_map_t[j] = 1
    #             else:
    #                 hash_map_t[j] += 1
    #
    #     if hash_map_s == hash_map_t:
    #         return True
    #     else:
    #         return False

    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        all_letters = list(set(s + t))
        for i in all_letters:
            if s.count(i) != t.count(i):
                return False
        return True


@pytest.mark.parametrize(
    's, t, result',
    (
            ('anagram', 'nagaram', True),
            ('rat', 'car', False),
    )
)
def test_solution(s, t, result):
    solution = Solution()

    assert solution.isAnagram(s, t) == result
