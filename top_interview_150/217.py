"""
Given an integer array nums, return true if any value appears at least twice in the array, and return false if every
element is distinct.
"""

import pytest
import typing as t


class Solution:
    # def containsDuplicate(self, nums: t.List[int]) -> bool:
    #     hash_map = dict()
    #
    #     for i in nums:
    #         if i not in hash_map:
    #             hash_map[i] = None
    #         else:
    #             return True
    #     return False

    def containsDuplicate(self, nums: t.List[int]) -> bool:
        hash_set = set()

        for i in nums:
            if i not in hash_set:
                hash_set.add(i)
            else:
                return True
        return False


@pytest.mark.parametrize(
    'nums, result',
    (
            ([1,2,3,1], True),
            ([1,2,3,4], False),
            ([1,1,1,3,3,4,3,2,4,2], True),
    )
)
def test_solution(nums, result):
    solution = Solution()

    assert solution.containsDuplicate(nums) == result
