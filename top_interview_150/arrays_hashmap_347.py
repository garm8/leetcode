"""
Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any
order.
"""

import typing as t
import pytest


class Solution:
    def topKFrequent(self, nums: t.List[int], k: int) -> t.List[int]:
        hash_map = dict()
        result = []

        for num in nums:
            if num not in hash_map:
                hash_map[num] = 1
            else:
                hash_map[num] += 1

        sorted_list = sorted(hash_map.items(), key=lambda x: x[1], reverse=True)
        for i in range(k):
            result.append(sorted_list[i][0])

        return result


@pytest.mark.parametrize(
    'nums, k, result',
    (
            ([1,1,1,2,2,3], 2, [1,2]),
            ([1], 1, [1]),

    )
)
def test_solution(nums, k, result):
    solution = Solution()

    assert solution.topKFrequent(nums, k) == result
