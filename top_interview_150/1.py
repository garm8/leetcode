"""
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to
target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.
"""

import typing as t
import pytest


class Solution:
    def twoSum(self, nums: t.List[int], target: int) -> t.List[int]:
        hash_map = dict()
        for i, num in enumerate(nums):
            delta = target - num
            if delta in hash_map:
                return [i, hash_map[delta]]
            hash_map[num] = i

    # def twoSum(self, nums: t.List[int], target: int) -> t.List[int]:
    #     for i, j in enumerate(nums):
    #         for k, f in enumerate(nums):
    #             if i == k:
    #                 continue
    #
    #             if j + f == target:
    #                 return [i, k]


@pytest.mark.parametrize(
    'nums, target, result',
    (
            ([3, 2, 3], 6, [0, 2]),
            ([5, 1, 2], 3, [1, 2]),
            ([2,7,11,15], 9, [0,1]),
            ([3,2,4], 6, [1,2]),
            ([3,3], 6, [0,1]),

    )
)
def test_solution(nums, target, result):
    solution = Solution()

    try:
        assert solution.twoSum(nums, target) == result
    except:
        assert solution.twoSum(nums, target) == result[::-1]
