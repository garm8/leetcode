"""
You are given two integer arrays nums1 and nums2, sorted in non-decreasing order, and two integers m and n, representing
 the number of elements in nums1 and nums2 respectively.

Merge nums1 and nums2 into a single array sorted in non-decreasing order.

The final sorted array should not be returned by the function, but instead be stored inside the array nums1. To
accommodate this, nums1 has a length of m + n, where the first m elements denote the elements that should be merged, and
 the last n elements are set to 0 and should be ignored. nums2 has a length of n.
"""


import typing as t
import pytest


class Solution:
    def merge(self, nums1: t.List[int], m: int, nums2: t.List[int], n: int) -> t.List[int]:
        print(nums1)
        for i in range(m):
            for j in range(n):
                # print(i, j, nums1[i], nums2[j])
                if nums1[i] < nums2[j]:
                    pass
                elif nums1[i] == nums2[j]:
                    nums1.insert(i+1, nums2[j])

                else:
                    continue

        print(nums1)
        print(nums2)
        result = [1, 2, 2, 3, 5, 6]
        return result


@pytest.mark.parametrize(
    'nums1, m, nums2, n, result',
    (
            ([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3, [1, 2, 2, 3, 5, 6]),
    )
)
def test_solution(nums1, m, nums2, n, result):
    solution = Solution().merge(nums1, m, nums2, n)
    assert solution == result


Solution().merge([1, 2, 3, 0, 0, 0], 3, [2, 5, 6], 3)
